tags-compacter
==============

`tags-compacter` is a small script for compacting a tags file in those rare and very specific cases when it needs to be compacted. If that makes no sense to you, then it's not your use case.
