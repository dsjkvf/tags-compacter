#!/usr/bin/env bash

# exit if already running
[ $(ps -axo comm | grep -c "[c]tags") -gt 0 ] && exit

# reset the previous instance
[ -f tags ] && rm tags
touch tags

# loop if error
while true
do
    # generate
    [ -x $HOME/usr/local/bin/ctags ] && $HOME/usr/local/bin/ctags --sort=yes *.$1 || ctags --sort=yes *.$1

    # clean if requested
    [ $# -eq 2 ] && awk -i inplace -v P=$2 '$0 !~ P' tags

    # uniq
    awk -i inplace -v FS='\t' '{a[$1]=$0} ENDFILE{PROCINFO["sorted_in"] = "@val_str_asc"; for (i in a) {print a[i]}}' tags

    # exit if no errors
    file --mime-encoding tags | grep -q binary || break
done
